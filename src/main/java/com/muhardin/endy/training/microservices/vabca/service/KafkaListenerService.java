package com.muhardin.endy.training.microservices.vabca.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.muhardin.endy.training.microservices.vabca.dto.TagihanRequest;
import com.muhardin.endy.training.microservices.vabca.dto.TagihanResponse;
import lombok.extern.slf4j.Slf4j;
import net.bytebuddy.utility.RandomString;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Random;

@Slf4j
@Service
public class KafkaListenerService {
    @Autowired private ObjectMapper objectMapper;
    @Autowired private KafkaSenderService kafkaSenderService;


    @KafkaListener(
            autoStartup = "${kafka.enabled:false}",
            topics = "${kafka.topic.tagihan.request:tagihan-request-dev}"
    )
    public void terimaTagihanRequest(String msg) throws JsonProcessingException {
        log.info("Terima message : {}", msg);
        TagihanRequest tr = objectMapper.readValue(msg, TagihanRequest.class);
        log.info("Tagihan request : {}", tr);

        TagihanResponse response = new TagihanResponse();
        BeanUtils.copyProperties(tr, response);

        // nantinya no va dihitung/generate sesuai proses bisnis
        // sekarang random saja
        response.setNomorVa("1234567890123456");
        kafkaSenderService.kirimTagihanResponse(response);

    }
}

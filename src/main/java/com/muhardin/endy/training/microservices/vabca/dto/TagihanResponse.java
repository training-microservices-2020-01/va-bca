package com.muhardin.endy.training.microservices.vabca.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class TagihanResponse {
    private String nomorTagihan;
    private String nama;
    private BigDecimal nilaiTagihan;
    private String bank = "BCA";
    private String nomorVa;
}

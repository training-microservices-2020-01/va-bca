package com.muhardin.endy.training.microservices.vabca.dto;

import lombok.Data;

import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class TagihanRequest {
    private String nomorTagihan;
    private String nama;
    private String email;
    private String noHp;
    private String keteranganTagihan;
    private BigDecimal nilaiTagihan;
    private LocalDate jatuhTempo;
}
